<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitce6f92c80182ccbd5eb740610a831a05
{
    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'Respect\\Validation\\' => 19,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Respect\\Validation\\' => 
        array (
            0 => __DIR__ . '/..' . '/respect/validation/library',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitce6f92c80182ccbd5eb740610a831a05::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitce6f92c80182ccbd5eb740610a831a05::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
