<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 02/02/2017
 * Time: 12:55
 */
require_once("Login.php");
require_once("DB.php");
class Contact
{
    public $id_contact, $nom, $prenom, $email, $adresse, $telephone, $id_login;

    function __construct( $nom, $prenom, $email, $adresse, $telephone, $id_login, $id_contact = "unknown")
    {
        $this->id_contact = $id_contact;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->adresse = $adresse;
        $this->id_login = $id_login;
    }

    /**
     * @return mixed
    */
    function _update()
    {
        $db = new DB();
        try
        {
            $db->statement = "UPDATE contact SET nom='".$this->nom."', prenom='".$this->prenom."', email='".$this->email."', telephone='".$this->telephone."', adresse='".$this->adresse."' WHERE id_contact =".$this->id_contact;

            $db->_go();
        }
        catch (PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }

        return "Contact mis à jour!";
    }
    function _new()
    {
        if($this->_exist()) return "Un contact similaire existe déja";
        var_dump($this->_exist());
        $db = new DB();
        try
        {
            $db->statement = "INSERT INTO contact (nom, prenom, email, telephone, adresse, id_login) VALUES('".$this->nom."','".$this->prenom."','".$this->email."','".$this->telephone."','".$this->adresse."',".$this->id_login.")";
            //var_dump($db->statement);
            $db->_go();
        }
        catch (PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }
        return "Nouveau Contact ajouté avec succes!";
    }
    function _delete()
    {
        $db = new DB();
        try
        {
            $db->statement = "DELETE FROM contact WHERE id_contact=".$this->id_contact;

            $db->_go();
        }
        catch (PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }
        return "Contact suprimme!";
    }
    function _exist()
    {
        $db = new DB();
        try
        {
            $db->statement = "SELECT * FROM contact WHERE nom ='".$this->nom."' AND prenom='".$this->prenom."'";

            $db->_selectContact();
        }
        catch(PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }
        //var_dump($_SESSION["contactList"]);
        if(count($_SESSION["contactList"]) > 0)return true;
        return false;
    }
}