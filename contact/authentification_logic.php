<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 02/02/2017
 * Time: 13:53
 */
require "vendor/autoload.php";
include_once 'Login.php';

session_start();
unset($_SESSION["login"]); //désétiquetage
use \Respect\Validation\Validator as v; // création d'alias

$_SESSION['error'] = ""; // resest du message

if(v::notBlank()->validate($_POST['motDePasse'])) //verification que le pseudo entré n'est pas vide
{
    if(v::notBlank()->validate($_POST['pseudo'])) // ou que le mot de passe ne l'est pas
    {
        $_POST['motDePasse'] =  hash("md5",$_POST['motDePasse']); //hashing du mot de passe
        $_SESSION["login"] = new Login($_POST['pseudo'], $_POST['motDePasse']);
        //création de l'obj user sur lequel on va effectuer des
        // vérifications implémentées dans la meme classe

        if ($_POST['submit'] == 'Connecter')
        {
            $_SESSION['error'] = $_SESSION["login"]->_existingUser();
        }
        else
        {

            $_SESSION['error'] = $_SESSION["login"]->_newUser();
        }
    }
    else $_SESSION['error'] = "Nom d'utilisateur invalide! ";
}
else $_SESSION['error'] .= "mot de passe invalide!";




//var_dump($_SESSION['error']);
//var_dump($_SESSION["login"]);
//
// //debug
//
// ci-desous
// Si les fonction de sign-in _existingUser() ou _newUser()
//ne renvoie pas d'erreur alors on est signed in et nos info
// d'utilisateur ont été stocké dans l' obj $_SESSION[login]
//
// //
if($_SESSION["error"] == "")
{
    header('Location: contact_view.php');
    exit();
}
else
{
    header('Location: auth_view.php');
    exit();
}