<?php
require "vendor/autoload.php";
include_once 'Contact.php';
include_once 'Login.php';

session_start();

$_SESSION["error"] = "";
$errorTag = true;
use \Respect\Validation\Validator as v;

$_SESSION["error"] = "";
if(v::alpha()->validate($_POST['telephone']))
{
    $_SESSION["error"] .= "numero de telephone ";
    $errorTag = false;
}
if(!v::email()->validate($_POST['email']))
{
    $_SESSION["error"] .= "adresse email ";
    $errorTag = false;

}
if(!v::alpha()->validate($_POST['nom']))
{
    $_SESSION["error"] .= "nom ";
    $errorTag = false;
}
if(!v::alpha()->validate($_POST['prenom']))
{
    $_SESSION["error"] .= "prenom ";
    $errorTag = false;
}
if($errorTag)
{
    if ($_POST['submit'] == 'Modifier')
    {
        $ContactWorkingON = $_SESSION["contactList"][$_SESSION["contact_selected_index"]];

        $ContactWorkingON->nom = $_POST['nom'];
        $ContactWorkingON->prenom = $_POST['prenom'];
        $ContactWorkingON->email = $_POST['email'];
        $ContactWorkingON->adresse = $_POST['adresse'];
        $ContactWorkingON->telephone = $_POST['telephone'];
        $_SESSION["error"] = $ContactWorkingON->_update();
    }

    elseif($_POST['submit'] == 'Creer')
    {
        $contact = new Contact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['adresse'], $_POST['telephone'], $_SESSION['login']->id_login);

        $_SESSION["error"] = $contact->_new();
    }
    else
    {
        $ContactWorkingON = $_SESSION["contactList"][$_SESSION["contact_selected_index"]];
        $_SESSION["error"] = $ContactWorkingON->_delete();
        $_SESSION["contact_selected_index"] = 0;
    }
    header("Location: contact_view.php");
    exit();
}
else
{
    $_SESSION["error"] .= "INCORRECT";
    header("Location: contact_view.php?nom=".$_POST['nom']."&prenom=".$_POST['prenom']."&email=".$_POST['email']."&adresse=".$_POST['adresse']."&telephone=".$_POST['telephone']);
    exit();
}

