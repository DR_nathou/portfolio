<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 02/02/2017
 * Time: 13:16
 */
include_once 'DB.php';
class Login
{
    public $pseudo, $motDePasse, $id_login;
    function __construct($pseudo, $motDePasse)
    {
        $this->pseudo = $pseudo;
        $this->motDePasse = $motDePasse;
        $this->id_login = "";
    }

    function _existingUser()
    {
        $db = new DB();
        try
        {
            if(!$this->_pseudoExist()) return "utilisateur non enregistré!";
            if($this->_pseudoExist() && !$this->_pwdIsValid()) return "mot de passe incorect!";

            $db->statement = "SELECT id_login FROM login WHERE pseudo ='".$this->pseudo."'";
            $this->id_login = $db->_selectIDlogin()[0]->id_login;
        }

        catch(PDOException $e)
        {
            echo $e . "vous n'existez pas";
        }
    }


    function  _newUser()
    {
        $db = new DB();
        try
        {
            if(!$this->_pseudoExist())
            {
                $db->statement = "INSERT INTO login(pseudo, mot_de_passe) VALUES ('" . $this->pseudo . "','" . $this->motDePasse . "')";
                $db->_go();
                $db->statement = "SELECT id_login FROM login WHERE pseudo ='" . $this->pseudo . "'";
                $this->id_login = $db->_selectIDLogin()[0]->id_login;
            }
            else return "pseudo déjà utilisé!";
        }
        catch(PDOException $e)
        {
            echo $e;
        }
    }

    function _pseudoExist()
    {
        $db = new DB();
        try
        {
            $db->statement = "SELECT * FROM login";
            $allLogin = $db->_selectLogin();
            for ($index = 0; $index < count($allLogin); $index++)
            {
                if ($this->pseudo == $allLogin[$index]->pseudo)
                {
                    return true;
                }
            }
            return false;
        }
        catch(PDOException $e)
        {
            echo $e;
        }
    }
    function _pwdIsValid()
    {
        $db = new DB();
        try
        {
            $db->statement = "SELECT mot_de_passe FROM login WHERE pseudo ='" . $this->pseudo . "'";
            //$dbpass = $db->_selectLogin();
            if($db->_selectLogin()[0]->mot_de_passe !== $this->motDePasse) return false;

        }
        catch(PDOException $e)
        {
            echo $e;
        }
        return true;
    }

}