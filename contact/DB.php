<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 02/02/2017
 * Time: 14:25
 */
include_once "Contact.php";
class DB
{
    private $connectionString, $result, $userName, $pwd;
    public $statement;

    /**
     * DB constructor.
     * @param string $statement
     * @param string $userName
     * @param string $pwd
     */
    function __construct($statement = "SELECT * FROM contact", $userName = 'root', $pwd = '')
    {
        $this->statement = $statement;
        $this->connectionString = 'mysql:host=localhost;dbname=contactsimple';
        $this->userName = $userName;
        $this->pwd = $pwd;
    }

    function _go()
    {
        try
        {
            $pdo = new PDO( $this->connectionString, $this->userName, $this->pwd);

            $this->result = $pdo->prepare($this->statement);
            $this->result->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    }
    function _selectContact()
    {
        $this->_go();
        $data = $this->result->fetchAll(PDO::FETCH_OBJ);
        $_SESSION["contactList"] = array();
        foreach ($data as $value)
        {
            $obj = new Contact($value->nom, $value->prenom, $value->email, $value->adresse, $value->telephone, $value->id_login, $value->id_contact);

            array_push($_SESSION["contactList"], $obj);
        }
    }
    function _selectIDLogin()
    {
        $this->_go();
        return $this->result->fetchAll(PDO::FETCH_OBJ);

    }
    function _selectLogin()
    {
        $this->_go();
        return $this->result->fetchAll(PDO::FETCH_OBJ);
    }

    function _selectAdress()
    {
        $this->_go();
        $data = $this->result->fetchAll(PDO::FETCH_OBJ);
        foreach ($data as $value)
        {
            $_SESSION["adresse"] = new Adresse($value->rue, $value->boite, $value->numero, $value->code_postal, $value->ville, $value->province, $value->pays);
        }
    }
    function _updateContact()
    {
        $this->_go();
    }

}