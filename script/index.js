/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function showPrev(img)
{
    prev = document.getElementById("preview");
    currentHeight = window.getComputedStyle(prev).getPropertyValue('height');
    
    if(currentHeight !== '0px')
    {
        setTimeout(function(){hidePrev();setTimeout(show(img),3000);},500);
    }
    else show(img);
}
function hidePrev()
{
    prev = document.getElementById("preview").style;

    prev.height = "0px";
    prev.transition =  "height 0.5s ease-in-out";
    prev.webkitTransition = "height 0.5s ease-in-out";
    prev.mozTransition = "height 0.5s ease-in-out";
    prev.oTransition = "height 0.5s ease-in-out";
    
}
function show(img)
{
    prev = document.getElementById("preview").style;
    prev.height = "inherit";
    switch (img) {
        case 'car':
            prev.background = "url('../img/carprev.png') no-repeat";
            break;
        case 'hanged':
            prev.background = "url('../img/hangedprev.png') no-repeat";
            break;
        case 'tfe':
            prev.background = "url('../img/tfeprev.png') no-repeat";
            break;
        case 'repair':
            prev.background = "url('../img/repairprev.png') no-repeat";
            break;
        case 'ascii':
            prev.background = "url('../img/asciiprev.png') no-repeat";
            break;
        case 'web':
            prev.background = "url('../img/webprev.png') no-repeat";
            break;
        case 'contact':
            prev.background = "url('../img/contact.png') no-repeat";
            break;
    }
    prev.transition = "height 2s ease-in-out";
    prev.webkitTransition = "height 2s ease-in-out";
    prev.mozTransition = "height 2s ease-in-out";
    prev.oTransition = "height 2s ease-in-out";
}

