/**
 * Created by drznathou on 09/12/2016.
 */
$(".side").on("click", "li", function(){
    $("#cube").removeClass().addClass($(this).attr("class"));
}).on("mouseover", "li", function(){
    var me   = $(this);
    var attr = me.attr("data-url");

    if(attr !== undefined){
        $("h2").text(attr).css("visibility", "visible");
    }
    
});