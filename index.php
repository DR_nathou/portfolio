<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/style/index.css" rel="stylesheet">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon">
    <script src="/script/index.js"></script>
    <title>H-O-M-E</title>
    
</head>
<body>

    <div id="header">
        <h1>Welcome</h1>
    </div>

    <div id="header1">
        <div class="dropdown">
            <button class="dropbtn">GIT</button>
            <div class="dropdown-content">
                <a onmouseover="showPrev('git')" onmouseout="hidePrev()"  href="https://DR_nathou@bitbucket.org/DR_nathou/rent4you.git">rent4you</a>
                <a onmouseover="showPrev('git')" onmouseout="hidePrev()"  href="https://DR_nathou@bitbucket.org/DR_nathou/contact_with_slim.git">Contact Slim</a>


            </div>
        </div>
        <div class="dropdown">
            <button class="dropbtn">Projet</button>
            <div class="dropdown-content">
                <a onmouseover="showPrev('tfe')" onmouseout="hidePrev()"  href="#">Gestion de cabinet médical</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="dropbtn">Exercices</button>
            <div class="dropdown-content">
                <a onmouseover="showPrev('ascii')" onmouseout="hidePrev()"  href="#">ASCII</a>
                <a onmouseover="showPrev('car')" onmouseout="hidePrev()"  href="/car/">Car driving</a>
                <a onmouseover="showPrev('hanged')" onmouseout="hidePrev()"  href="/hanged/">Hanged man</a>
                <a onmouseover="showPrev('contact')" onmouseout="hidePrev()"  href="/contact/">AdresseBook</a>
            </div>

        </div>
        <div class="dropdown">
            <button class="dropbtn">Service</button>
            <div class="dropdown-content">
                <a onmouseover="showPrev('repair')" onmouseout="hidePrev()" href="#">Pc maintenance</a>
                <a onmouseover="showPrev('web')" onmouseout="hidePrev()" href="#">web</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="dropbtn" onclick="location.href='contact.php';">contact</button>
        </div>

    </div>

    <div id="central">
        <p>
            Ce site regroupe mes travaux principaux, Vous trouverez également mes services.
        </p>
        <div id="preview"></div>
    </div>   
</body>
</html>