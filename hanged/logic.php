<?php session_start();

function endgame($won = true)
{
   if($won)
   {
       $_SESSION["title"] = "Félicitations, quelle performance!";
       $_SESSION["sound"] = "/hanged/sound/".$_SESSION["theme"]."/win.wav";
   }
   else
   {
       $_SESSION["title"] = "Looser... le mot était: ". $_SESSION["word"] ."!";
       $_SESSION["sound"] = "/hanged/sound/".$_SESSION["theme"]."/lose.wav";
   }
}



if (!empty($_GET["letter"])
    && $_SESSION["lives"] > 0
    && !strpos($_SESSION["letters"], $_GET["letter"])
    && $_SESSION["word"] != $_SESSION["answer"])
{
    $_SESSION["letters"] .= $_GET["letter"];

    if (!strpos($_SESSION["word"], $_GET["letter"])) {
        $_SESSION["lives"] -= 1;
        $_SESSION['progress'] += $_SESSION['step']*1;
    }

    for ($i = 0; $i < strlen($_SESSION["word"]); $i++)
    {
        if ($_SESSION["word"][$i] == $_GET["letter"]) $_SESSION["answer"][$i] = $_GET["letter"];
    }
    if($_SESSION["word"] == $_SESSION["answer"]) endgame();
    elseif($_SESSION["lives"] <= 0) endgame(false);
}
header("Location: http://".$_SERVER['HTTP_HOST']."/hanged/game.php");
exit();