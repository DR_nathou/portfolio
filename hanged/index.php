<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 10/11/2016
 * Time: 17:53
 * INIT PAGE
 */
session_start();
define("HEAD" ,'game.php');
$lines = file('./files/dic.txt');
$_SESSION["word"] = trim($lines[rand(1,count($lines)-1)]);
$_SESSION["letters"] = " ";
$_SESSION["answer"] = "";
$_SESSION["lives"] = strlen($_SESSION["word"]);
$_SESSION["step"] = round(100/$_SESSION["lives"], 0 ,PHP_ROUND_HALF_UP);
$_SESSION['progress'] = 0;
//comment
$_SESSION["bg"] = "/hanged/img/bg.png";
$_SESSION["bgWidth"] = getimagesize($_SESSION["bg"])["width"];
$_SESSION["bgHeight"] = getimagesize($_SESSION["bg"])["height"];

$_SESSION["sound"] = "/hanged/sound/clic.wav";

$_SESSION["title"] = "Trouve le mot suivant pour annuler l'installation de wundoz!";

for ($i = 0;$i < (strlen($_SESSION["word"])); $i++)
{
    $_SESSION["answer"] .= '_';
}

    header("Location: http://".$_SERVER['HTTP_HOST']."/hanged/game.php");
__DIR__
;
    exit();

