<?php session_start();
//L'affichage?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="/hanged/style/theme1.css" rel="stylesheet">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>hanged</title>
</head>
<body>

<audio src="<?php echo $_SESSION["sound"];?>" autoplay="autoplay"></audio>

<div id="screen">
    <span id="title"><?php echo $_SESSION["title"]?></span>
    <div id="answer">
    <?php

    for ($i = 0;$i < strlen($_SESSION["answer"]); $i++)
    {
        echo $_SESSION["answer"][$i] . "&nbsp;&nbsp;";
    }
    echo "<br>".$_SESSION["letters"]."<br>";
    ?>
    </div>
    <?php if($_SESSION['progress']>90){ echo '<img src="/hanged/img/progress/0.png">'; $_SESSION['progress'] = 100;}?>
    <div id="progress">
        <img src="/hanged/img/progress/6.png">

        <div id="txt">
            <h5><?php echo $_SESSION["progress"]?>%</h5>
        </div>
        <div id="bar">
            <progress value="<?php if($_SESSION['progress']>90) echo 100; else echo $_SESSION['progress'];?>" max="100" ></progress>

        </div>
    </div>
</div>
<div id="pc">
    <img id="pc" src="<?php echo $_SESSION["bg"]?>"
         width="<?php echo $_SESSION["bgWidth"]?>"
         height="<?php echo $_SESSION["bgHeight"]?>"
         border="0" usemap="#theme1" />

    <map name="theme0">
        <area shape="poly" coords="372,209,402,210,407,232,374,232" href="/hanged/logic.php?letter=u"/>
        <area shape="poly" coords="236,209,267,209,265,232,232,231" href="/hanged/logic.php?letter=e" />
        <area shape="poly" coords="202,209,232,209,228,232,196,231" href="/hanged/logic.php?letter=w" />
        <area shape="poly" coords="407,209,437,209,442,233,410,232" href="/hanged/logic.php?letter=i" />
        <area shape="poly" coords="441,209,470,209,479,233,447,232" href="/hanged/logic.php?letter=o" />
        <area shape="poly" coords="337,209,369,209,371,231,339,231" href="/hanged/logic.php?letter=y" />
        <area shape="poly" coords="167,209,197,209,191,232,159,231" href="/hanged/logic.php?letter=q" />
        <area shape="poly" coords="306,210,335,210,335,231,305,232" href="/hanged/logic.php?letter=t" />
        <area shape="poly" coords="270,210,301,210,300,232,270,232" href="/hanged/logic.php?letter=r" />
        <area shape="poly" coords="476,209,505,209,514,233,482,232" href="/hanged/logic.php?letter=p" />
        <area shape="poly" coords="310,237,344,237,344,266,310,266" href="/hanged/logic.php?letter=g" />
        <area shape="poly" coords="275,237,307,237,306,265,273,265" href="/hanged/logic.php?letter=f" />
        <area shape="poly" coords="239,236,272,236,269,265,234,265" href="/hanged/logic.php?letter=d" />
        <area shape="poly" coords="202,236,234,236,229,265,196,265" href="/hanged/logic.php?letter=s" />
        <area shape="poly" coords="167,236,199,236,191,266,158,265" href="/hanged/logic.php?letter=a" />
        <area shape="poly" coords="347,237,380,237,381,265,347,266" href="/hanged/logic.php?letter=h" />
        <area shape="poly" coords="384,237,416,237,420,266,386,265" href="/hanged/logic.php?letter=j" />
        <area shape="poly" coords="419,237,453,237,459,266,423,266" href="/hanged/logic.php?letter=k" />
        <area shape="poly" coords="456,237,490,237,496,266,460,266" href="/hanged/logic.php?letter=l" />
        <area shape="poly" coords="404,268,439,268,443,299,405,298" href="/hanged/logic.php?letter=m" />
        <area shape="poly" coords="364,268,400,268,403,299,365,299" href="/hanged/logic.php?letter=n" />
        <area shape="poly" coords="326,268,362,268,364,298,327,298" href="/hanged/logic.php?letter=b" />
        <area shape="poly" coords="288,268,323,268,321,298,286,298" href="/hanged/logic.php?letter=v" />
        <area shape="poly" coords="251,268,286,268,284,298,247,299" href="/hanged/logic.php?letter=c" />
        <area shape="poly" coords="212,268,247,268,243,298,206,298" href="/hanged/logic.php?letter=x" />
        <area shape="poly" coords="173,268,208,268,202,298,165,298" href="/hanged/logic.php?letter=z" />
        <area shape="poly" coords="160,183,178,183,170,205,146,205,149,192,156,187" href="/hanged/index.php?theme=theme0" />
        <area shape="poly" coords="182,182,210,182,203,205,175,206,174,204" href="/hanged/index.php?theme=theme1" />
        <area shape="poly" coords="510,209,545,209,555,232,519,233" href="/hanged/index.php?theme=theme0" />
        <area shape="poly" coords="610,301,648,301,670,341,628,340" href="/hanged/index.php?theme=theme0" />
        <area shape="poly" coords="213,182,241,182,237,205,209,205" href="/hanged/index.php?theme=theme0" />
    </map>
    <map name="theme1">
        <area shape="rect" coords="43,92,99,129" href="/hanged/logic.php?letter=a" />
        <area shape="rect" coords="115,94,170,127" href="/hanged/logic.php?letter=z" />
        <area shape="rect" coords="189,93,243,128" href="/hanged/logic.php?letter=e" />
        <area shape="rect" coords="261,93,316,128" href="/hanged/logic.php?letter=r" />
        <area shape="rect" coords="335,93,389,129" href="/hanged/logic.php?letter=t" />
        <area shape="rect" coords="407,93,461,129" href="/hanged/logic.php?letter=y" />
        <area shape="rect" coords="480,93,535,129" href="/hanged/logic.php?letter=u" />
        <area shape="rect" coords="552,93,607,129" href="/hanged/logic.php?letter=i" />
        <area shape="rect" coords="626,93,680,129" href="/hanged/logic.php?letter=o" />
        <area shape="rect" coords="699,94,753,127" href="/hanged/logic.php?letter=p" />
        <area shape="rect" coords="75,152,130,187" href="/hanged/logic.php?letter=q" />
        <area shape="rect" coords="148,153,200,189" href="/hanged/logic.php?letter=s" />
        <area shape="rect" coords="220,152,274,187" href="/hanged/logic.php?letter=d" />
        <area shape="rect" coords="292,153,346,187" href="/hanged/logic.php?letter=f" />
        <area shape="rect" coords="366,153,421,187" href="/hanged/logic.php?letter=g" />
        <area shape="rect" coords="438,153,493,188" href="/hanged/logic.php?letter=h" />
        <area shape="rect" coords="512,153,564,188" href="/hanged/logic.php?letter=j" />
        <area shape="rect" coords="585,153,637,188" href="/hanged/logic.php?letter=k" />
        <area shape="rect" coords="657,153,710,189" href="/hanged/logic.php?letter=l" />
        <area shape="rect" coords="119,213,173,246" href="/hanged/logic.php?letter=w" />
        <area shape="rect" coords="191,212,244,247" href="/hanged/logic.php?letter=x" />
        <area shape="rect" coords="264,211,319,247" href="/hanged/logic.php?letter=c" />
        <area shape="rect" coords="336,211,391,248" href="/hanged/logic.php?letter=v" />
        <area shape="rect" coords="410,213,464,247" href="/hanged/logic.php?letter=b" />
        <area shape="rect" coords="482,212,536,246" href="/hanged/logic.php?letter=n" />
        <area shape="rect" coords="555,212,609,248" href="/hanged/logic.php?letter=m" />
        <area shape="rect" coords="12,34,68,70" href="/hanged/index.php?theme=theme0" />
        <area shape="rect" coords="85,33,141,69" href="/hanged/index.php?theme=theme1" />
        <area shape="rect" coords="157,34,213,70" href="/hanged/index.php?theme=theme1" />
        <area shape="rect" coords="702,213,786,247" href="/hanged/index.php?theme=theme1" />
    </map>
    <map name="theme2">
        <area shape="rect" coords="131,97,160,116" href="/hanged/logic.php?letter=s" />
        <area shape="rect" coords="98,97,127,116" href="/hanged/logic.php?letter=a" />
        <area shape="rect" coords="388,76,417,95" href="/hanged/logic.php?letter=p" />
        <area shape="rect" coords="356,76,385,95" href="/hanged/logic.php?letter=o" />
        <area shape="rect" coords="324,76,353,95" href="/hanged/logic.php?letter=i" />
        <area shape="rect" coords="292,76,321,95" href="/hanged/logic.php?letter=u" />
        <area shape="rect" coords="260,75,289,94" href="/hanged/logic.php?letter=y" />
        <area shape="rect" coords="227,75,256,94" href="/hanged/logic.php?letter=t" />
        <area shape="rect" coords="196,75,225,94" href="/hanged/logic.php?letter=r" />
        <area shape="rect" coords="163,75,192,94" href="/hanged/logic.php?letter=e" />
        <area shape="rect" coords="131,75,160,94" href="/hanged/logic.php?letter=w" />
        <area shape="rect" coords="292,120,321,139" href="/hanged/logic.php?letter=m" />
        <area shape="rect" coords="260,120,289,139" href="/hanged/logic.php?letter=n" />
        <area shape="rect" coords="227,120,256,139" href="/hanged/logic.php?letter=b" />
        <area shape="rect" coords="195,120,224,139" href="/hanged/logic.php?letter=v" />
        <area shape="rect" coords="163,119,192,138" href="/hanged/logic.php?letter=c" />
        <area shape="rect" coords="130,119,159,138" href="/hanged/logic.php?letter=x" />
        <area shape="rect" coords="98,119,127,138" href="/hanged/logic.php?letter=z" />
        <area shape="rect" coords="357,98,386,117" href="/hanged/logic.php?letter=l" />
        <area shape="rect" coords="324,98,353,117" href="/hanged/logic.php?letter=k" />
        <area shape="rect" coords="292,98,321,117" href="/hanged/logic.php?letter=j" />
        <area shape="rect" coords="260,98,289,117" href="/hanged/logic.php?letter=h" />
        <area shape="rect" coords="227,98,256,117" href="/hanged/logic.php?letter=g" />
        <area shape="rect" coords="195,97,224,116" href="/hanged/logic.php?letter=f" />
        <area shape="rect" coords="163,97,192,116" href="/hanged/logic.php?letter=d" />
        <area shape="rect" coords="99,75,128,94" href="/hanged/logic.php?letter=q" />
        <area shape="rect" coords="420,54,449,73" href="/hanged/index.php" />
        <area shape="rect" coords="419,31,448,50" href="/hanged/index.php" />
        <area shape="rect" coords="99,31,128,50" href="/hanged/index.php?theme=theme0" />
        <area shape="rect" coords="132,31,161,50" href="/hanged/index.php?theme=theme1" />
        <area shape="rect" coords="163,31,192,50" href="/hanged/index.php" />
    </map>

</div>
<audio id="audio" src="" ></audio>
</body>
</html>