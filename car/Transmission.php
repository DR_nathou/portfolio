<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transmission
 *
 * @author drznathou
 */
class Transmission 
{
    public $position;
    public $maxSpeedPerPosition;
    public $maxPosition;
    public $autoTransmission;
    //put your code here
    function __construct($maxPosition = 5, $autoTransmission = false)
    {
        if($autoTransmission)
        {
            $this->maxSpeedPerPosition = array(-1 => 20, 0 => 0, 1 => 150);
            $this->position = 0;
            $this->maxPosition = $maxPosition;
        }
        else
        {
            $this->maxSpeedPerPosition = array(-1 => 20, 0 => 0, 1 => 20, 2 => 40, 3 => 70, 4 => 100, 5 => 140, 6 => 240);
            $this->position = 0;
            $this->maxPosition = $maxPosition;
        }
        $this->autoTransmission = $autoTransmission;
    }

    
    function _switch($up = true) 
    {
        if($up && $this->position < $this->maxPosition)
        {
            $this->position ++;
            return $up;
        }
        elseif (!$up && $this->position > -1) 
        {
            $this->position --;
            return $up;
        }
        else
        {
            if ($up) { return "Vous etes déjà en ".$this->position.".<br> C'est votre position maximum.";}
            else {return "Vous etes déjà en marche arrière!";}
        }
    }
    
}
