<?php

require_once 'Voiture.php';
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/car/style/css.css" rel="stylesheet">
    <title>- P I M P -</title>
    <script type="text/javascript">
        function toggle_visibility(id) {
            var e = document.getElementById(id);
            if(e.style.display == 'block')
                e.style.display = 'none';
            else
                e.style.display = 'block';
        }
    </script>
</head>
<body>

<div id="car">
    <img src="/car/img/car.png" width="1440" height="960" border="0" usemap="#map" />
    <map name="map">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$AUTHOR:drznathou -->
        <area shape="circle" coords="356,482,33" href="/car/gearsup.php?action=honkSwitch" />
        <area shape="poly" coords="782,794,785,787,797,682,798,661,798,656,801,639,802,624,
        805,595,805,590,805,588,803,584,802,583,802,573,803,571,804,568,804,567,798,567,793,
        567,792,567,792,568,792,569,793,570,794,570,794,582,790,584,790,588,790,602,787,660,
        786,665,786,667,770,789,772,794,777,797" href="/car/gearsup.php?action=handbrakeSwitch" />
        <area shape="poly" coords="506,587,564,586,569,655,565,660,514,662,512,661,512,660,505,592"
              href="/car/gearsup.php?action=speedDown" />
        <area shape="poly" coords="648,570,613,739,616,744,622,747,637,751,645,750,651,748,
        652,739,687,573,682,569,676,565,663,561,654,562,652,563" href="/car/gearsup.php?action=speedUp" />
        <area shape="circle" coords="288,429,15" href="/car/gearsup.php?action=start" />
        <area shape="poly" coords="730,548,789,546,789,552,789,556,788,561,784,565,779,570,773,
        574,766,577,756,577,748,575,743,572,735,566,730,556,729,548" href="/car/gearsup.php?action=changeTransmission(false)" />
        <area shape="poly" coords="729,547,789,545,788,542,787,539,784,534,781,531,775,525,764,
        522,752,522,743,525,739,528,733,536,730,541" href="/car/gearsup.php?action=changeTransmission" />
    </map>
</div>
<div id="road">
    <img src="/car/img/road/<?php switch ($_SESSION['car']->speed)
    {
        case 0:
        {
            echo "stop";
            break;
        }
        case ($_SESSION['car']->speed<25 && $_SESSION['car']->transmission->position == -1):
        {
            echo "back";
            break;
        }
        case ($_SESSION['car']->speed<25):
        {
            echo "0";
            break;
        }
        case ($_SESSION['car']->speed<50):
        {
            echo "1";
            break;
        }
        case ($_SESSION['car']->speed<75):
        {
            echo "2";
            break;
        }
        case ($_SESSION['car']->speed<100):
        {
            echo "3";
            break;
        }
        case ($_SESSION['car']->speed<150):
        {
            echo "5";
            break;
        }
        case ($_SESSION['car']->speed<200):
        {
            echo "6";
            break;
        }
        case ($_SESSION['car']->speed>200):
        {
            echo "7";
            break;
        }
    }?>.gif">
</div>


<div id="msg">

    <?php   echo "nom: " .$_SESSION["car"]->name;
            if($_SESSION["car"]->transmission->autoTransmission)
            {
                switch($_SESSION["car"]->transmission->position)
                {
                    case -1:
                        echo "<br> marche arriere";
                        break;
                    case 0:
                        echo "<br> point mort";
                        break;
                    case 1:
                        echo "<br> marche avant";
                        break;
                }
            }
            else echo "<br> boite de vitesse: ". $_SESSION["car"]->transmission->position;
             echo "<br> vitesse: " .
            $_SESSION["car"]->speed . "<br>";

            echo $_SESSION["msg"];?>
    <br>
    <button onclick="toggle_visibility('pimp')" href="#pimp">pimp my ride</button>
    <div id="pimp">
        <form method="post" action="index.php">
            <label for="name">Model:</label>
            <input type="text" name="name">
            <br>
            <label for="autoTransmission">Boite:</label>
            <input type="radio" name="autoTransmission" onclick="document.getElementById('typeDeBoite').style.display = 'none'"value="TRUE" checked> Auto
            <input type="radio" name="autoTransmission" onclick="document.getElementById('typeDeBoite').style.display = 'block'" value="FALSE"> Manuelle
            <br>
            <div id="typeDeBoite">
                <label for="autoTransmission">Type de boite:</label>
                <input type="radio" name="maxTransmissionPosition" value="5" checked> 5
                <input type="radio" name="maxTransmissionPosition" value="6"> 6
            </div>
            <input type="submit" name="action" value="Go Go Go">
        </form>
    </div>
</div>
</body>
</html>