<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'Transmission.php';
/**
 * 
 * Description of Voiture
 *
 * @author drznathou
 */
class Voiture 
{
    public $name;
    public $engineState;
    public $power;
    public $honk;
    public $transmission;
    public $speed;
    public $handbrake;
    
    
    function __construct($name, $power = 5, $maxTransmissionPosition = 5, $autoTransmission = false)
    {
        $this->transmission = new Transmission($maxTransmissionPosition, $autoTransmission);
        $this->engineState = false;
        $this->honk = false;
        $this->name = $name;
        $this->power = $power;
        $this->window = false;
        $this->speed = 0;
        $this->handbrake = true;
    }
    
    function honkSwitch()
    {
        if($this->honk) $this->honk = true;
        else $this->honk = true;
    }

    /**
     * @return string
     */
    function start()
    {
        if(!$this->engineState)
        {
            if ($this->handbrake == true)
            {
                $this->speed = 0;
                $this->engineState = true;
                return "Attention le frein à <br>main est enclenché!";
            }
            elseif ($this->transmission->position != -1 || $this->transmission->position != 1)
            {
                $this->speed = 0;
                $this->engineState = true;
                return "Moteur démarer!<br> passez en 1ere pour avancer<br>ou en en marche arriere.";
            }
            else

                $this->speed = $this->power;
                $this->engineState = true;
                return "Vous roulez à " . $this->speed;
            }

        else
        {
            $this->engineState = false;
            return 'moteur éteint';
        }
    }
    function speedUp()
    {
        if($this->engineState)
        {
            if($this->handbrake == true)
            {
                return "Désenclanchez le<br>frein à main!";
            }
            elseif($this->transmission->position == 0) return "Mettez vous en 1ere pour avancer";
            elseif($this->transmission->position == -1 && $this->speed <= $this->transmission->maxSpeedPerPosition[-1])
            {
                $this->speed = $this->transmission->maxSpeedPerPosition[-1];
                return "Vitesse max en<br>marche arriere";
            }
            else return $this->checkTransmissionPosition();

        }
        else return 'Allumez le moteur dabord';
    } 
    function speedDown()
    {
        if($this->engineState)
        {
            $this->speed -= round($this->speed / 2);
            return "hiii" . $this->speed;
        }
        else return 'Allumer le moteur dabord.' ;
    }
    function changeTransmission($up = true)
    {
        $this->transmission->_switch($up);
    }
    function handbrakeSwitch()
    {
        if ($this->handbrake) 
        {
            $this->handbrake = false; 
            return "Frein a main désenclenché";

        }
        else
        {
            $this->handbrake = true;
            $this->speed = 0;
            return "Frein a main enclenché";
        }
    }
    private function checkTransmissionPosition()
    {
        if(($this->speed + $this->power + 10) > $this->transmission->maxSpeedPerPosition[$this->transmission->position])
        {
            $this->speed = $this->transmission->maxSpeedPerPosition[$this->transmission->position];
            if(!($this->transmission->position+1) == $this->transmission->maxPosition)
                return "Passez en ".($this->transmission->position+1)." pour accelerer.";
            else return "Vitesse maximum atteinte!";
        }
        else
        {
            $this->speed += 10 + $this->power;
            return "Quelle puissance!".$this->speed;
        }
    }
     
}
