/**
* Created by PhpStorm.
* User: drznathou
* Date: 05/12/2016
* Time: 09:01
*/

<?php

require_once 'Voiture.php';
session_destroy();
session_start();

if(isset($_POST['autoTransmission']))
{
    if($_POST['name'] == '')
    {
        $_SESSION["msg"] = "Model invalide";
        header("Location: contact_view.php");
        exit();
    }
    if ($_POST['autoTransmission'] == "TRUE") $_POST['autoTransmission'] = true;
    else $_POST['autoTransmission'] = false;
    $_SESSION["car"] = new Voiture($_POST['name'], 5,$_POST['maxTransmissionPosition'], $_POST['autoTransmission']);
}
else
{
    $_SESSION["car"] = new Voiture("Carrera", 10, 6, false);
}
$_SESSION["msg"] = "";


header("Location: contact_view.php");
exit();