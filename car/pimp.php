<?php
/**
* Created by PhpStorm.
* User: drznathou
* Date: 05/12/2016
* Time: 09:01
*/
if(isset($_GET['name']))
{
    $name = $_GET['name'];
    $power = $_GET['power'];
}
else
{
    $name = '';
    $power = '';
    $maxTransmissionPosition = '';
}
?>
<form method="post" action="/TOP/index.php">
    <label for="name">Model:</label>
    <input type="text" name="name" value="<?php echo $name?>">

    <label for="power">Indice de puissance (0-10):</label>
    <input type="range" min="0" max="10" name="power" value="<?php echo $power?>">

    <label for="autoTransmission">Boite:</label>
    <input type="radio" name="autoTransmission" value="TRUE" checked> Automatique<br>
    <input type="radio" name="autoTransmission" value="FALSE"> Manuelle<br>

    <label for="autoTransmission">Nombre de postion dans la boite de vitesses:</label>
    <input type="radio" name="maxTransmissionPosition" value="5" checked> 5<br>
    <input type="radio" name="maxTransmissionPosition" value="6"> 6<br>

    <input type="submit" name="action" value="Go Go Go">
</form>