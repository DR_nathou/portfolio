<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 18/12/2016
 * Time: 17:35
 */
session_abort();
session_start();
require 'Timer.php';
require 'Ring.php';
require 'Plot.php';
$_SESSION['timer'] = new Timer();
$_SESSION['SwapRing'] = new Ring();
$_SESSION['selectedPlot'] = 0;
$_SESSION['ConsoleColor'] = array('Yellow', 'Green', 'Cyan', 'Blue', 'Magenta', 'Red');
$_SESSION['message'] = "SPACE          = select\n\rLEFT and RIGHT = move\n\rR              = rules";
$_SESSION['numberOfRings'] = 6;
$_SESSION['numberOfMoves'] = 0;
$_SESSION['PlotList'] = array(new Plot(true), new Plot(), new Plot());




$_SESSION['timer']->start();




echo $_SESSION['timer']->elapsed();
