<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 18/12/2016
 * Time: 17:35
 */
session_start();
require 'Ring.php';
class Plot
{
    public $ringList;
    function __construct($fill = false)
    {
        $this->ringList = array();
        if($fill)
        {
            for ($index = 0; $index < $_SESSION['numberOfRings']; $index++)
            {
                array_push($this->ringList, new Ring($index, $_SESSION['ConsoleColor'][$index]));
            }
        }
    }
    function addRing($destinationPlot)
    {
        array_push($destinationPlot->ringList, $this->ringList[0]);
        array_splice($this->ringList[0], 1, 1);
    }
}