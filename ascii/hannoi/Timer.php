<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 18/12/2016
 * Time: 17:38
 */
class Timer
{
private $startTimeHour;
private $startTimeMinute;
private $startTimeSecond;
private $startTimeMiliSecond;
private $stopTimeHour;
private $stopTimeMinute;
private $stopTimeSecond;
private $stopTimeMiliSecond;

function __construct()
{
    $this->startTimeMiliSecond =  2222;
    $this->startTimeSecond =  22;
    $this->startTimeMinute =  22;
    $this->startTimeHour =  22;
    $this->stopTimeMiliSecond =  9999;
    $this->stopTimeSecond =  99;
    $this->stopTimeMinute =  99;
    $this->stopTimeHour =  99;
}

function start()
{
    $this->startTimeMiliSecond =  date('U');
    $this->startTimeSecond =  date('s');
    $this->startTimeMinute =  date('i');
    $this->startTimeHour =  date('H');
}

function stop()
{
    $this->stopTimeMiliSecond = date('U');
    $this->stopTimeSecond = date('s');
    $this->stopTimeMinute = date('i');
    $this->stopTimeHour = date('H');
}

function elapsed()
{
    $resultHour = $this->stopTimeHour - $this->startTimeHour;
    $resultMin = $this->stopTimeMinute - $this->startTimeMinute;
    $resultSecond = $this->stopTimeSecond - $this->startTimeSecond;
    $resultMiliSecond = $this->stopTimeMiliSecond - $this->startTimeMiliSecond;
        if ($this->startTimeMiliSecond > $this->stopTimeMiliSecond)
        {
            $resultSecond++;
        }
        if ($this->startTimeMinute > $this->stopTimeMinute)
        {
            $resultHour++;
        }
        if ($this->startTimeSecond > $this->stopTimeSecond)
        {
            $resultMin++;
        }

    return ($resultHour .":".
        $resultMin .":".
        $resultSecond .".".
        $resultMiliSecond);
}

}